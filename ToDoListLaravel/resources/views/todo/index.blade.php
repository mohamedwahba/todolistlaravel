@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center">My To Do list</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{route('todo.store')}}" method="post">
            @csrf
            <input type="text" name="name" class="form-control mb-3">
            <input type="submit" value="add" class="btn btn-success form-control">
        </form>

    </div>
    <table class="table table-hover container text-center">
        <tr>
            <th>todo</th>
            <th colspan="2">option</th>

        </tr>
        @foreach($all_todos as $todo)
            <tr>

                <td>{{$todo->name}}</td>
                <td><a href="{{route('todo.edit',$todo->id)}}" class="btn btn-success">Edit</a></td>

                <td>
                    <form action="{{route('todo.destroy',$todo->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
                @if(!$todo->completed)
                    <td><a  href="{{url('/todo/completed',$todo->id)}}" class="btn btn-success">Mark as Completed</a></td>
                @else
                  <td ><p class="alert alert-success">Completed</p></td>
                @endif
            </tr>
        @endforeach

    </table>


@endsection