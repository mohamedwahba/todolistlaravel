<?php

namespace App\Http\Controllers;

use App\todo;
use Illuminate\Http\Request;

class todoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
       $all_todos = $user->todos()->get();
        return view('todo.index',['all_todos'=>$all_todos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate($request,[
            'name'=>'required',
        ]);
        $data['user_id']=auth()->user()->id;
        todo::create($data);
//        dd($data);
//        $todo = new todo();
//        $todo->name=request('name');
//        $todo->user_id = auth()->user()->id;
//        $todo->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo = todo::find($id);
        return view('todo.edit',['todo'=>$todo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->validate($request,[
            'name'=>'required',
        ]);
        $todo = todo::find($id)->update($data);
        return redirect()->route('todo.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        todo::findOrfail($id)->delete();
        return redirect()->route('todo.index');
    }

    public function completed($id){
       $todo = todo::find($id);
       $todo->completed =1;
       $todo->save();
       return back();
    }
}
